'use strict'

const { parse } = require('node-html-parser')
const rp = require('request-promise')
const selenium = require('./selenium')

const pingZara = (url) => {
    return selenium.getHTMLData(url)
    .then(response => {
        const root = parse(String(response))
        const items = root.querySelectorAll('li[role="option"]')
        
        if(!items) throw(new Error('Invalid product URL'))
        
        return items.map(item => !item.classNames.includes('size-selector__size-list-item--is-disabled'))
    })
}

const fetchProductSizes = (url) => {
    return selenium.getHTMLData(url)
    .then(response => {
        const root = parse(String(response))
        const sizes = root.querySelectorAll('li[role="option"]')
        return sizes.map(el => String(el.childNodes[0].childNodes[0].childNodes[0].rawText).trim())
    })
}

module.exports = {
    pingZara,
    fetchProductSizes,
}