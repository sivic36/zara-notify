FROM node:14.17.6-alpine
WORKDIR /zara-notify
COPY prisma/schema.prisma prisma/
COPY package.json .
COPY package-lock.json .
COPY *.js ./
RUN npm install
RUN npx prisma migrate dev --name init
ENTRYPOINT ["npm", "start"]