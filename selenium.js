const {Builder, Browser, By, Capabilities} = require('selenium-webdriver');
const chrome = require("selenium-webdriver/chrome");
require('dotenv').config()

const getHTMLData = async (url) => {
    const driver = new Builder()
      .usingServer(process.env.SELENIUM_WEBDRIVER_SERVER)
      .forBrowser('chrome')
      .build();
    await driver.get(url);
    const sizes = await driver.findElement(By.css('.size-selector__size-list'));
    const result = await sizes.getAttribute("innerHTML")
    await driver.quit();
    return result;
}

module.exports = {
  getHTMLData
}