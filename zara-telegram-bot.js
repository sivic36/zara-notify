const Telegraf = require("telegraf"); // import telegraf lib
const Markup = require("telegraf/markup"); // Get the markup module
const Stage = require("telegraf/stage");
const WizardScene = require("telegraf/scenes/wizard");
const session = require("telegraf/session");
const zaraApi = require('./zara-api')
const db = require('./zara-db')
const { URL } = require('url');
const { removeKeyboard } = require("telegraf/markup");
require('dotenv').config()

const bot = new Telegraf(process.env.BOT_TOKEN); // Get the token from the environment variable

bot.start(ctx => {
  ctx.reply('Send /zarazena to get started 💸')
})

bot.command('zarazena', ctx => {
  ctx.reply(`How can I help you, ${ctx.from.first_name}?`, getMenuMarkup())
});

bot.action("BACK", ctx => {
  ctx.reply(`How can I help you, ${ctx.from.first_name}?`, getMenuMarkup())
});

bot.action("VIEW_PRODUCTS", async (ctx) => {
  db.getAllUserProductSubs(ctx.chat.id)
  .then(subs => {
    try {
      ctx.reply(
        formatUserProductSubs(subs),
        Markup.inlineKeyboard([
          Markup.callbackButton("⬅️ Back to Menu", "BACK"),
          Markup.callbackButton("❌ Delete a product", "DELETE_PRODUCT")
        ]).extra({parse_mode: 'html'}),
      );
    }
    catch {
      throw new Error()
    }
  })
  .catch(() => {
    ctx.reply('Something went wrong 😢\nTry again later.', getMenuMarkup())
  })
});

const deleteProduct = new WizardScene(
  "delete_product",
  ctx => {
    // GET SUB ID
    ctx.wizard.state.chatId = ctx.chat.id
    ctx.reply("Send me a Product Subscription (#) you want to remove:");
    return ctx.wizard.next();
  },

  async (ctx) => {
    try {
      db.getSubscription(Number(ctx.message.text))
      .then(sub => {
        if (sub && sub.chatId === ctx.wizard.state.chatId) {
          return db.removeSubscription(sub)
        }
        else {
          throw new Error()
        }
      })
      .then(() => {
        ctx.reply(
          'This product subscription is deleted 😊',
          Markup.inlineKeyboard([
            Markup.callbackButton("⬅️ Back to Menu", "BACK"),
            Markup.callbackButton("❌ Delete one more", "DELETE_PRODUCT")
          ]).extra()
        );
        return ctx.scene.leave();
      })
      .catch(err => {
        ctx.reply(err.message ? err.message : 'Invalid subscription ID 😢\nPlease try again.', getMenuMarkup())
        return ctx.scene.leave();
      })
    }
    catch (err) {
      ctx.reply('Invalid subscription ID 😢\nPlease try again.', getMenuMarkup())
      return ctx.scene.leave();
    }
  }
);

const notificationBuilder = new WizardScene(
  "notification_builder",
  ctx => {
    // GET PRODUCT URL
    ctx.wizard.state.chatId = ctx.chat.id
    ctx.reply("Send me ZARA product URL you're interested about:");
    return ctx.wizard.next();
  },

  async (ctx) => {
    try {
      const url = await validateURL(ctx.message.text)
      if(!url) {
        ctx.reply('Error while parsing URL content 😢\nPlease try again or contact support.', getMenuMarkup())
        return ctx.scene.leave();
      }
  
      // GET PRODUCT SIZE INDEX
      else {
        ctx.wizard.state.url = url
        ctx.wizard.state.options = await zaraApi.fetchProductSizes(ctx.wizard.state.url)
        ctx.reply('Got it, what size you\'re looking for?', getSizeMarkup(ctx.wizard.state.options))
        return ctx.wizard.next();
      }
    }
    catch (err) {
      console.error(err)
      return ctx.scene.leave();
    }
  },
  ctx => {
    try {
      const sizeIndex = ctx.wizard.state.options.indexOf(ctx.message.text)
      if (sizeIndex < 0) {
        ctx.reply('Invalid size 😢\nPlease try again.', getMenuMarkup())
        return ctx.scene.leave();
      }
      
      // START NOTIFICATION SERVICE
      else {
        ctx.wizard.state.sizeIndex = sizeIndex
        const {options, ...data} = ctx.wizard.state
        db.createProductSubscription(data)
        .then(() => {
          ctx.reply(
            'OK, that\'s it. Now sit back and relax, I\'ll send you a message when it\'s available.\nYou\'re welcome 😊',
            Markup.inlineKeyboard([
              Markup.callbackButton("⬅️ Back to Menu", "BACK"),
              Markup.callbackButton("🔔 I need one more", "NOTIFY_ME")
            ]).extra()
          );
        })
        .catch((err) => {
          let msg = 'You already have this combination 😕\nPlease try again.'
          if (err.code === 400) {
            msg = 'Invalid URL 😕\nPlease try again.'
          }
          ctx.reply(msg, getMenuMarkup())
        })
        return ctx.scene.leave();
      }
    }
    catch (err) {
      return ctx.scene.leave();
    }
  }
);

// Scene registration
const stage = new Stage([notificationBuilder, deleteProduct]);
bot.use(session());
bot.use(stage.middleware());

bot.action("NOTIFY_ME", async (ctx) => {
  await ctx.scene.enter("notification_builder")
});

bot.action("DELETE_PRODUCT", async (ctx) => {
  await ctx.scene.enter("delete_product")
});

bot.startPolling();

// HELPER functions
async function validateURL (url) {
  try {
    var safeURL = new URL(url)
    // await zaraApi.fetchProductSizes(url)
    return safeURL.origin
      .concat(safeURL.pathname)
      .concat(`?v1=${safeURL.searchParams.get('v1')}`)
  }
  catch (err) {
    console.error(err)
    return
  } 
}

function formatUserProductSubs(subs) {
  let strings = '';
  subs.forEach(sub => {
    strings += `<b># ${sub.id}</b> \n`
    strings += `You wanted this in size ${sub.sizeIndex}:\n${sub.product.url}\n\n`
  })
  if (subs.length < 1) {
    strings = 'You don\'t have any products yet. Start adding some!'
  }
  return String(strings)
}

function getMenuMarkup () {
  return Markup.inlineKeyboard([
    Markup.callbackButton("🔔 I need to get notified", "NOTIFY_ME"),
    Markup.callbackButton("👗 Show me my products", "VIEW_PRODUCTS")
  ]).extra()
}

function getSizeMarkup (options) {
  const buttons = []
  options.forEach(option => {
      buttons.push([{text: option}])
  })
  return {reply_markup: {keyboard: buttons, one_time_keyboard: true}}
}

const notifyUser = (data) => {
  const {url, chatId} = data;
  bot.telegram.sendMessage(chatId, `This product is now available! Go, hurry up!\n${url}`)
}

module.exports = {
  notifyUser
}