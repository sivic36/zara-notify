'use strict'

const { PrismaClient } = require('@prisma/client')
const prisma = new PrismaClient()

const createProductSubscription = async (data) => {
    const product = await prisma.product.findUnique({
        where: { id: getProductId(data.url) }
    })

    // Add client subscription to existing product
    if (product) {
        await prisma.subscription.create({
            data: {
                chatId: data.chatId,
                sizeIndex: data.sizeIndex,
                product: {
                    connect: { id: product.id }
                }
            }
        })
    }

    // Create new product with client subscription
    else {
        await prisma.product.create({
            data: {
                id: getProductId(data.url),
                url: data.url,
                subscriptions: {
                    create: {
                        chatId: data.chatId,
                        sizeIndex: data.sizeIndex
                    }
                }
            }
        })
    }
    await prisma.$disconnect()
}

const getSubscription = async(subId) => {
    const result = await prisma.subscription.findUnique({
        where: {
            id: subId,
        }
    })
    await prisma.$disconnect()
    return result
}

const getAllProducts = async () => {
    const data = await prisma.product.findMany({
        include: {
            subscriptions: true
        }
    })
    await prisma.$disconnect()
    return data
}

const getAllUserProductSubs = async (chatId) => {
    const result = await prisma.subscription.findMany({
        where: {
            chatId
        },
        include: {
            product: true
        }
    })
    await prisma.$disconnect()
    return result
}

const removeSubscription = async (data) => {
    await prisma.subscription.delete({
        where: {
            subId: {
                chatId: data.chatId,
                productId: data.productId,
                sizeIndex: data.sizeIndex,
            }
        }
    })
    await prisma.$disconnect()
}

const removeProduct = async (product) => {
    await prisma.product.delete({
        where: { id: getProductId(product.url) }
    })
    await prisma.$disconnect()
}

// HELPER functions
function getProductId(url) {
    const p = url.split('.html')[0].slice(-9)
    const urlObject = new URL(url)
    let v = urlObject.searchParams.get('v1');
    if (p) {
        if (!v) {
            v = 0;
        }
        return p + v;
    }
    else {
        const err = new Error('Invalid URL')
        err.code = 400
        throw(err)
    }
}

module.exports = {
    createProductSubscription,
    getAllProducts,
    removeSubscription,
    removeProduct,
    getAllUserProductSubs,
    getSubscription,
}