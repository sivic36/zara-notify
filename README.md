# Zara Notify

<img src="docs/zara-notify-logo.png" title="Zara Notify logo" width="100" height="100">

**Zara Notify** - Node.js service for sending notification about ZARA products availability to clients via Telegram Chat Bot.

## Prerequisites

Before you run application, you should visit [Telegram pages](https://core.telegram.org/bots) where the process of creating your own chat bot is explained.

When you create your own `Telegram Chat Bot`, you should get it's `TOKEN` and have your `.env` file in the root of the project with the following environment variables defined:
```YAML
BOT_TOKEN="YOUR_TELEGRAM_CHAT_BOT_TOKEN"
SELENIUM_WEBDRIVER_SERVER=http://localhost:4444/wd/hub
PING_ZARA_PERIOD='*/2 * * * *'
```

**Selenium webdriver server** could be started locally with `docker`:
```
docker run --name selenium-node -p 4444:4444 selenium/standalone-chrome:latest
```

There is [.env.sample](/.env.sample) and [docker-compose.sample.yml](/deploy/docker-compose.sample.yml) file provided for running the service.

## Getting started

After cloning the repo, install the required dependencies:

```shell
npm install
```

Then, run the following to sync prisma scheme and generate prisma client:

```shell
npx prisma migrate dev
```

After that you can run the application with:
```
npm start
```