const { pingZara } = require('./zara-api')
const { notifyUser } = require('./zara-telegram-bot')
const db = require('./zara-db')
const cron = require('node-cron')
require('dotenv').config()

cron.schedule(process.env.PING_ZARA_PERIOD, () => {
    db.getAllProducts()
    .then(products => {
        products.forEach(product => {
            if(product.subscriptions.length === 0) {
                db.removeProduct(product)
            }
            else {
                pingZara(product.url)
                .then(items => {
                    product.subscriptions.forEach(sub => {
                        if(items[sub.sizeIndex]) {
                            console.log(`Available size ${sub.sizeIndex} for URL: ${product.url}`)
                            notifyUser({url: product.url, chatId: sub.chatId})
                            db.removeSubscription(sub)
                        }
                    })
                })
                .catch(err => {
                    console.error("Error while pinging Zara product: ", product.url);
                    console.error("Error status code: " + err.statusCode)
                    console.info("Removing the product: " + product.url);
                    db.removeProduct(product);
                })
            }
        })
    })
    .catch(console.log)
});